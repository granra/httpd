#include <assert.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <arpa/inet.h>
#include <time.h>

#define NR_CONNECTIONS 30
#define TIMEOUT 5

struct request_line {
    char method[8]; /* Longest allowed method is 7 letters */
    char target[200];
    char version[10];
};

struct connection {
    int fd;
    char host[15];
    int port;
    int idle_time;
};

/* Takes in a connection, reads the available data, parses it and responds
 * Returns: 1 if the connection should close, 0 otherwise */
int handle_request(struct connection* conn);
/* Takes in a HTTP request message and parses it. The request line goes into
 * a struct. The Headers are parsed into a GHashTable.
 * Returns: A pointer to a c-string containing the body. If there is no body
 * NULL is returned */
char* fetch_request(gchar* message, struct request_line* req, GHashTable* headers);
/* Takes in a URI and parses it for parameters and stores them as a key value
 * pair in GHashTable.
 * Example: target = "/?parameter=val&color=red"
 * Returns: GHashTable* if parameters were found, NULL otherwise. */
GHashTable* fetch_parameters(char* target);
/* Takes in data about the request and builds the response string
 * Returns: A pointer to the response string */
char* create_response(struct request_line* request,
                        GHashTable* headers,
                        gchar* data,
                        struct connection* conn,
                        int* status);
/* Takes in a cookie string from a request header and parses the wanted cookie
 * from it.
 * Returns: A pointer to the cookie value string */
char* parse_cookie(char* cookie_string, char* cookie_name);
void log_request(struct connection* conn, struct request_line* request, int status);

int main(int argc, char** argv)
{
    int sockfd, i, max_fd;
    struct connection* connections[NR_CONNECTIONS];
    struct sockaddr_in server, client;

    if (argc < 2) return 1;

    memset(connections, 0, NR_CONNECTIONS * sizeof(struct connection*));

    /* Create and bind a UDP socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    /* Network functions need arguments in network byte order instead of
       host byte order. The macros htonl, htons convert the values, */
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(strtol(argv[1], NULL, 0));
    if (bind(sockfd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) == -1) {
        printf("Bind error\n");
        return 1;
    }

	/* Before we can accept messages, we have to listen to the port. We allow one
	 * 1 connection to queue for simplicity.
	 */
	listen(sockfd, 1);

    for (;;) {
        fd_set rfds;
        struct timeval tv;
        int retval;

        /* Check whether there is data on the socket fd. */
        FD_ZERO(&rfds);
        /* Add the master socket to the set */
        FD_SET(sockfd, &rfds);

        /* Initialize max fd as sockfd */
        max_fd = sockfd;

        /* Add all active connections to the set */
        for (i = 0; i < NR_CONNECTIONS; ++i) {
            struct connection* conn = connections[i];

            if (conn != NULL) {
                FD_SET(conn->fd, &rfds);
                if (conn->fd > max_fd) max_fd = conn->fd;
            }
        }

        /* Wait for five seconds. */
        tv.tv_sec = TIMEOUT;
        tv.tv_usec = 0;
        retval = select(max_fd + 1, &rfds, NULL, NULL, &tv);

        if (retval == -1) {
                perror("select()");
        } else if (retval > 0) {
            /* If data was sent to the master socket a new connection
             * is being initialized */
            if (FD_ISSET(sockfd, &rfds)) {
                int connfd;
                socklen_t len = (socklen_t) sizeof(client);

                connfd = accept(sockfd, (struct sockaddr*)&client, &len);

                /* Create a new struct to store information about connection */
                struct connection* conn = malloc(sizeof(struct connection));
                conn->fd = connfd;
                conn->port = ntohs(client.sin_port);
                conn->idle_time = 0;
                strcpy(conn->host, inet_ntoa(client.sin_addr));

                if (handle_request(conn)) {
                    shutdown(connfd, SHUT_RDWR);
                    close(connfd);
                    g_free(conn);
                } else {
                    /* Add new connection to our array */
                    for (i = 0; i < NR_CONNECTIONS; ++i) {
                        if (connections[i] == NULL) {
                            connections[i] = conn;
                            break;
                        }
                    }
                }
            }

        } else {
            fprintf(stdout, "No message in five seconds.\n");
            fflush(stdout);
        }

        /* Check all connections */
        for (i = 0; i < NR_CONNECTIONS; ++i) {
            struct connection* conn = connections[i];
            int since_last = TIMEOUT - tv.tv_sec;

            if (conn != NULL) {
                int should_close = 0;
                if (FD_ISSET(conn->fd, &rfds)) {
                    should_close = handle_request(conn);
                    conn->idle_time = 0;
                } else {
                    conn->idle_time += since_last;
                }

                if (should_close || conn->idle_time >= 30) {
                    shutdown(conn->fd, SHUT_RDWR);
                    close(conn->fd);
                    g_free(conn);
                    connections[i] = NULL;
                }
            }
        }
    }
}

int handle_request(struct connection* conn) {
    size_t n;
    int ret = 1, status;
    struct request_line request;
    gchar *body = NULL, *response = NULL, *buffer = g_new0(gchar, 1024);
    GHashTable* headers = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                g_free, g_free);

    /* If we read in 0 bytes we should close */
    if ((n = read(conn->fd, buffer, 1023)) > 0) {
        buffer[n] = '\0';
        body = fetch_request(buffer, &request, headers);

        response = create_response(&request, headers, body, conn, &status);
        write(conn->fd, response, (size_t) strlen(response));

        log_request(conn, &request, status);

        gchar* connection = g_hash_table_lookup(headers, "Connection");

        if (connection != NULL && !strncmp(connection, "keep-alive", 10)) ret = 0;
    }

    /* Cleanup */
    g_hash_table_destroy(headers);
    /* g_free is NULL safe */
    g_free(body);
    g_free(response);
    g_free(buffer);

    return ret;
}

gchar* fetch_request(gchar* message, struct request_line* req, GHashTable* headers) {
    int i;
    gchar* ret = NULL;

    /* Split the request on CRLF so we can look at each line separately */
    gchar** heads = g_regex_split_simple("\\r\\n", message, 0, 0);

    /* Split the request line on space to parse each part */
    gchar** request = g_regex_split_simple("\\s", heads[0], 0, 0);

    /* Populate the request line struct */
    strcpy(req->method, request[0]);
    strcpy(req->target, request[1]);
    strcpy(req->version, request[2]);

    GRegex* regex = g_regex_new("([^:]+):\\s*(.+)", 0, 0, NULL);

    for (i = 1; heads[i] != NULL; ++i) {
        gchar *key, *value;
        GMatchInfo* match_info;
        g_regex_match(regex, heads[i], 0, &match_info);

        if (g_match_info_matches(match_info)) {
            /* Fetch the key and value from the regex */
            key = g_match_info_fetch(match_info, 1);
            value = g_match_info_fetch(match_info, 2);

            /* Insert it into the hash table */
            g_hash_table_insert(headers, (gpointer) key, (gpointer) value);
        }

        g_match_info_free(match_info);
    }

    if (strlen(heads[--i]) > 0) ret = strdup(heads[i]);

    /* cleanup */
    g_regex_unref(regex);
    g_strfreev(heads);
    g_strfreev(request);

    return ret;
}

GHashTable* fetch_parameters(char* target) {
    GHashTable* parameters = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                    g_free, g_free);

    GRegex* regex = g_regex_new("([^&^?]+)=([^?^&]+)", 0, 0, NULL);
    GMatchInfo* match_info;
    g_regex_match(regex, target, 0, &match_info);

    while (g_match_info_matches(match_info)) {
        gchar *key, *value;
        key = g_match_info_fetch(match_info, 1);
        value = g_match_info_fetch(match_info, 2);

        g_hash_table_insert(parameters, (gpointer) key, (gpointer) value);

        g_match_info_next(match_info, NULL);
    }

    /* Cleanup */
    g_regex_unref(regex);
    g_match_info_free(match_info);
    if (g_hash_table_size(parameters) < 1) {
        g_hash_table_destroy(parameters);
        parameters = NULL;
    }

    return parameters;
}

char* create_response(struct request_line* request,
                        GHashTable* headers,
                        gchar* data,
                        struct connection* conn,
                        int* status) {
    GHashTable* parameters = fetch_parameters(request->target);
    GHashTable* resHeaders = g_hash_table_new_full(g_str_hash, g_str_equal,
                                                    g_free, g_free);
    GString* response = g_string_new(NULL);
    GString* htmlBuilder = g_string_new("<!DOCTYPE html><html>");
    gchar* host = g_hash_table_lookup(headers, "Host");
    gpointer key = NULL, value = NULL;
    GHashTableIter iter;

    /* Set necessary response headers */
    key = g_malloc(sizeof "Content-Type" + 1);
    sprintf(key, "Content-Type");
    value = g_malloc(sizeof "text/html; charset=UTF-8" + 1);
    sprintf(value, "text/html; charset=UTF-8");
    g_hash_table_insert(resHeaders, (char*)key, (char*)value);
    key = g_malloc(sizeof "Server" + 1);
    sprintf(key, "Server");
    value = g_malloc(sizeof "Awesome Server/0.1" + 1);
    sprintf(value, "Awesome Server/0.1");
    g_hash_table_insert(resHeaders, (char*)key, (char*)value);
    key = NULL; value = NULL;


    if (!strncmp(request->method, "GET", 3) || !strncmp(request->method, "HEAD", 4)) {
        /* Route: /color */
        if (!strncmp(request->target, "/color", 6)) {
            gchar* color = NULL;
            gchar* cookies = NULL;
            if (parameters != NULL) color = g_hash_table_lookup(parameters, "bg");

            if (color == NULL) {
                cookies = g_hash_table_lookup(headers, "Cookie");

                if (cookies != NULL) {
                    color = parse_cookie(cookies, "bg");
                }
            }

            /* Build HTML */
            if (color == NULL) {
                /* Neither the bg parameter nor a cookie was provided */
                g_string_append(htmlBuilder, "<body>Use the bg parameter to set background color");
            } else {
                g_string_append_printf(htmlBuilder, "<body style=\"background-color: %s\">", color);
            }
            g_string_append(htmlBuilder, "</body></html>");

            /* Set status code */
            *status = 200;

            /* Set bg cookie */
            if (color != NULL) {
                key = g_malloc(sizeof "Set-Cookie" + 1);
                sprintf(key, "Set-Cookie");
                value = g_malloc(sizeof "bg= " + strlen(color));
                sprintf(value, "bg=%s", color);
                g_hash_table_insert(resHeaders, (char*)key, (char*)value);
                key = NULL; value = NULL;
            }

            /* If cookies is NULL no parameter was provided and
             * the cookie was checked. We should free color */
            if (cookies != NULL) g_free(color);

        } else {
            /* Building HTML */
            g_string_append(htmlBuilder, "<body>");

            g_string_append_printf(htmlBuilder, "http://%s%s<br />", host, request->target);
            g_string_append_printf(htmlBuilder, "%s:%d<br />", conn->host, conn->port);

            /* Display the headers on the page */
            g_string_append(htmlBuilder, "<br /><b><i>Headers</i></b><br />");
            g_hash_table_iter_init(&iter, headers);
            while (g_hash_table_iter_next(&iter, &key, &value)) {
                g_string_append_printf(htmlBuilder, "<b>%s</b>: %s<br />", (gchar*) key, (gchar*) value);
            }

            /* Display, if any, the parameters on the page */
            if (parameters != NULL) {
                g_string_append(htmlBuilder, "<br /><b><i>Parameters</i></b><br />");
                g_hash_table_iter_init(&iter, parameters);
                while (g_hash_table_iter_next(&iter, &key, &value)) {
                    g_string_append_printf(htmlBuilder, "%s=%s<br />", (gchar*) key, (gchar*) value);
                }
            }

            g_string_append(htmlBuilder, "</body></html>");

            /* Set status code */
            *status = 200;
        }
    } else if (!strncmp(request->method, "POST", 4)) {
        /* Building HTML */
        g_string_append(htmlBuilder, "<body>");

        g_string_append_printf(htmlBuilder, "http://%s%s<br />", host, request->target);
        g_string_append_printf(htmlBuilder, "%s:%d<br />", conn->host, conn->port);
        g_string_append_printf(htmlBuilder, "<b>Data:</b><br />%s<br />", data);

        g_string_append(htmlBuilder, "</body></html>");

        /* Set status code */
        *status = 200;
    }

    g_string_append_printf(response, "HTTP/1.1 %d OK\r\n", *status);

    /* Insert response headers into response */
    g_hash_table_iter_init(&iter, resHeaders);
    while (g_hash_table_iter_next(&iter, &key, &value)) {
        g_string_append_printf(response, "%s: %s\r\n", (char*)key, (char*)value);
    }

    g_string_append_printf(response, "Content-Length: %d\r\n", (int)strlen(htmlBuilder->str));
    g_string_append(response, "\r\n");
    /* HEAD response shouldn't include html body */
    if (strcmp("HEAD", request->method)) {
        g_string_append(response, htmlBuilder->str);
    }

    /* Cleanup */
    if (parameters != NULL) g_hash_table_destroy(parameters);
    g_hash_table_destroy(resHeaders);
    g_string_free(htmlBuilder, 1);

    return g_string_free(response, 0);
}

char* parse_cookie(char* cookie_string, char* cookie_name) {
    char* regex_string = g_malloc(strlen(cookie_name) + sizeof "=([^;]+)\\s*");
    sprintf(regex_string, "%s=([^;]+)\\s*", cookie_name);
    char* ret = NULL;

    GRegex* regex = g_regex_new(regex_string, 0, 0, NULL);
    GMatchInfo* match_info;

    g_regex_match(regex, cookie_string, 0, &match_info);
    if (g_match_info_matches(match_info)) {
        ret = g_match_info_fetch(match_info, 1);
    }

    /* Cleanup */
    g_free(regex_string);
    g_regex_unref(regex);
    g_match_info_free(match_info);

    return ret;
}

void log_request(struct connection* conn, struct request_line* request, int status) {
    time_t now;
    time(&now);
    char timestring[sizeof "YYYY-MM-DDTHH:MM:SSZ"];

    strftime(timestring, sizeof timestring, "%FT%TZ", gmtime(&now));
    FILE* f = fopen("httpd.log", "a");
    fprintf(f, "%s : %s:%d %s %s : %d\n", timestring, conn->host, conn->port,
            request->method, request->target, status);
    fclose(f);
}
